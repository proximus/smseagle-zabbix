Zabbix-SMS-EAGLE
================

Plugin for Zabbix to send SMS Alerts with SMSEagle device (https://www.smseagle.eu)

Script source: https://bitbucket.org/proximus/smseagle-zabbix/

Published on BSD License


Installation instructions
-------------------------

#### SMSEAGLE SETUP

Create a new user for this script in SMSEagle.


#### ZABBIX SETUP

1. Download latest version of Zabbix script from: https://bitbucket.org/proximus/smseagle-zabbix


2. Edit following lines in the script:

    //Set the following three values:
    $smseagle_ip     = "192.168.0.102";
    $login     = "smseagleuser";
    $password     = "smseaglepassword";


3. Put the script in the directory, you specified in the zabbix_server.conf, key AlertScriptsPath:
 Location for custom alert scripts AlertScriptsPath=/etc/zabbix/alertscripts</code> and ensure that it's executable (chmod 755 cli_smseagle.php).


4. Test the script, by running:
	./cli_smseagle.php 0048123456789 "Test message"

	After a few seconds, you should receive an SMS.


5. In Zabbix goto Administration > Media Types > Add a new media. Fill in required fields:

   - Name "SMS via SMSEagle" (or any other description)
   - Type "Script" 
   - Script name "cli_smseagle.php" 
   - Script parameters: add parameters  {ALERT.SENDTO} and {ALERT.MESSAGE} 
   - check "Enabled"

6. Finally, in Zabbix, Administration, Users, click on a user, and add a new media called "SMS via SMSEagle". Enter destination phone number, and press Save.


